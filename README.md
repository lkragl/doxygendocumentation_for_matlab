# DoxygenDocumentation_for_Matlab
Adapted code of Fabrice's [Using Doxygen with Matlab](https://de.mathworks.com/matlabcentral/fileexchange/25925-using-doxygen-with-matlab) for Linux systems.

## Requirements:

	sudo apt-get install doxygen doxygen-doc doxygen-gui graphviz 

## Usage:
Call the following in the repository:
	
	doxygen Doxyfile
